package com.binary_studio.tree_max_depth;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}

		int size = 1;
		List<Department> listNodes = rootDepartment.subDepartments.stream().filter(Objects::nonNull)
				.collect(Collectors.toList());

		while (!listNodes.isEmpty()) {
			listNodes = listNodes.stream().flatMap(department -> department.subDepartments.stream())
					.filter(Objects::nonNull).collect(Collectors.toList());
			size++;
		}
		return size;
	}

}
