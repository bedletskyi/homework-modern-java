package com.binary_studio.uniq_in_sorted_stream;

import java.util.stream.Stream;

public final class UniqueSortedStream {

	private static long previousItem;

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		return stream.filter(item -> {
			if (item.getPrimaryId().equals(previousItem)) {
				return false;
			}
			previousItem = item.getPrimaryId();
			return true;
		});
	}

}
