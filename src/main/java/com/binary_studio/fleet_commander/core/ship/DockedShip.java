package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {

		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.pg = powergridOutput;
		this.capacitor = capacitorAmount;
		this.capacitorRegeneration = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.attackSubsystem = null;
			return;
		}
		if (checkFitSubsystem(subsystem)) {
			this.attackSubsystem = subsystem;
		}
		else {
			throw new InsufficientPowergridException(this.pg.value());
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = null;
			return;
		}
		if (checkFitSubsystem(subsystem)) {
			this.defenciveSubsystem = subsystem;
		}
		else {
			throw new InsufficientPowergridException(this.pg.value());
		}
	}

	private boolean checkFitSubsystem(Subsystem subsystem) {
		int subsystemPowerConsumption = 0;
		if (subsystem instanceof AttackSubsystem) {
			subsystemPowerConsumption = this.defenciveSubsystem == null ? 0
					: this.defenciveSubsystem.getPowerGridConsumption().value();
		}
		else if (subsystem instanceof DefenciveSubsystem) {
			subsystemPowerConsumption = this.attackSubsystem == null ? 0
					: this.attackSubsystem.getPowerGridConsumption().value();
		}
		int powerConsumptionAllSubsystem = subsystemPowerConsumption + subsystem.getPowerGridConsumption().value();

		return powerConsumptionAllSubsystem <= this.pg.value();
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return new CombatReadyShip(this);
	}

	public String getName() {
		return this.name;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public PositiveInteger getCapacitor() {
		return this.capacitor;
	}

	public PositiveInteger getCapacitorRegeneration() {
		return this.capacitorRegeneration;
	}

	public PositiveInteger getPg() {
		return this.pg;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public PositiveInteger getSpeed() {
		return speed;
	}

	public PositiveInteger getSize() {
		return size;
	}

}
