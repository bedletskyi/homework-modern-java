package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final DockedShip ship;

	private PositiveInteger currentShieldHP;

	private PositiveInteger currentHullHP;

	private PositiveInteger currentCapacitor;

	public CombatReadyShip(DockedShip ship) {
		this.ship = ship;
		this.currentCapacitor = ship.getCapacitor();
		this.currentShieldHP = ship.getShieldHP();
		this.currentHullHP = ship.getHullHP();
	}

	@Override
	public void endTurn() {
		int regenerateCapacitorValue = this.currentCapacitor.value() + ship.getCapacitorRegeneration().value();
		this.currentCapacitor = regenerateCapacitorValue >= ship.getCapacitor().value() ? ship.getCapacitor()
				: PositiveInteger.of(regenerateCapacitorValue);
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return ship.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return ship.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return ship.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		int capacitorValue = this.currentCapacitor.value()
				- ship.getAttackSubsystem().getCapacitorConsumption().value();
		if (capacitorValue < 0) {
			return Optional.empty();
		}
		this.currentCapacitor = PositiveInteger.of(capacitorValue);

		return Optional.of(
				new AttackAction(ship.getAttackSubsystem().attack(target), this, target, ship.getAttackSubsystem()));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction reduceAttackAction = ship.getDefenciveSubsystem().reduceDamage(attack);
		int realDamage = reduceAttackAction.damage.value();

		if (this.currentShieldHP.value() - realDamage < 0) {
			this.currentShieldHP = PositiveInteger.of(0);
			realDamage = realDamage - this.currentShieldHP.value();
		}
		else {
			this.currentShieldHP = PositiveInteger.of(this.currentShieldHP.value() - realDamage);
			realDamage = 0;
		}

		if (this.currentHullHP.value() - realDamage < 0) {
			this.currentHullHP = PositiveInteger.of(0);
			return new AttackResult.Destroyed();
		}
		else {
			this.currentHullHP = PositiveInteger.of(this.currentHullHP.value() - realDamage);
			return new AttackResult.DamageRecived(reduceAttackAction.weapon, reduceAttackAction.damage,
					reduceAttackAction.target);
		}
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		int capacitorValue = this.currentCapacitor.value()
				- ship.getDefenciveSubsystem().getCapacitorConsumption().value();
		if (capacitorValue < 0) {
			return Optional.empty();
		}
		this.currentCapacitor = PositiveInteger.of(capacitorValue);

		RegenerateAction regenerateValue = ship.getDefenciveSubsystem().regenerate();
		PositiveInteger hullRegenerated = PositiveInteger.of(0);
		PositiveInteger shieldRegenerated = PositiveInteger.of(0);

		if (this.currentHullHP.value() < ship.getHullHP().value()) {
			int regenerateSum = this.currentHullHP.value() + regenerateValue.hullHPRegenerated.value();
			if (regenerateSum < ship.getHullHP().value()) {
				this.currentHullHP = PositiveInteger.of(regenerateSum);
				hullRegenerated = regenerateValue.hullHPRegenerated;
			}
		}

		if (this.currentShieldHP.value() < ship.getShieldHP().value()) {
			int regenerateSum = this.currentShieldHP.value() + regenerateValue.shieldHPRegenerated.value();
			if (regenerateSum < ship.getShieldHP().value()) {
				this.currentShieldHP = PositiveInteger.of(regenerateSum);
				shieldRegenerated = regenerateValue.shieldHPRegenerated;
			}
		}

		return Optional.of(new RegenerateAction(shieldRegenerated, hullRegenerated));
	}

}
